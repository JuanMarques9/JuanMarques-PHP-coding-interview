<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\controllers\Booking;

class BookingTest extends TestCase {
	private $booking;
	
	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->booking = new Booking();
	}

	/** @test */
	public function getBookings() {
		$results = $this->booking->getBookings();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);

		$this->assertEquals($results[0]['id'], 1);
		$this->assertEquals($results[0]['clientid'], 1);
		$this->assertEquals($results[0]['price'], 200);
		$this->assertEquals($results[0]['checkindate'], '2021-08-04 15:00:00');
		$this->assertEquals($results[0]['checkoutdate'], '2021-08-11 15:00:00');
	}

	/** @test */
	public function createBooking(){

		$newBooking = [
			'checkindate' => '2021-08-04 15:00:00',
			'checkoutdate' => '2021-08-11 15:00:00',
			'price' => 200,
			'clientid' => 1
		];
		
		$newBooking = $this->booking->addBooking($newBooking);

		$results = $this->booking->getBookings();

		$index = $newBooking['id'] - 1;
		$this->assertIsArray($results);
		$this->assertIsNotObject($results);
		$this->assertEquals($results[$index]['id'], $newBooking['id']);
		$this->assertEquals($results[$index]['clientid'], $newBooking['clientid']);
		$this->assertEquals($results[$index]['price'], $newBooking['price']);
		$this->assertEquals($results[$index]['checkindate'], $newBooking['checkindate']);
		$this->assertEquals($results[$index]['checkoutdate'], $newBooking['checkoutdate']);

	}
}