<?php

namespace Src\models;

use Src\helpers\Helpers;

class BookingModel {

	private $bookingData;
	private $helper;

	function __construct() {
		$this->helper = new Helpers();
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/bookings.json');
		$this->bookingData = json_decode($string, true);
	}

	public function getBookings() {
		return $this->bookingData;
	}

	public function addBooking($bookingData){
		$bookingData['id'] = end($this->bookingData)['id'] + 1;
		$this->bookingData[] = $bookingData;
		$this->helper->putJson($this->bookingData, 'bookings');
		return $bookingData;
	}
}