<?php

namespace Src\controllers;

use Src\models\BookingModel;

class Booking {

	private function getBookingModel(): BookingModel {
		return new BookingModel();
	}

	public function getBookings() {
		return $this->getBookingModel()->getBookings();
	}

	public function addBooking($data) {		
		$checkoutDate = $data['checkoutdate'];
		$checkinDate = $data['checkindate'];
		$price = $data['price'];
		$clientId = $data['clientid'];

		$booking = [
			'checkindate' => $checkinDate,
			'checkoutdate' => $checkoutDate,
			'price' => $price,
			'clientid' => $clientId
		];

		$newBook = $this->getBookingModel()->addBooking($booking);

		return $newBook;

	}
}